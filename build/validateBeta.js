const { exit } = require('process');
const fs = require('fs');
const path = require('path');

function load(fileName) {
    const filePath = path.join(__dirname, '..', fileName);
    return JSON.parse(fs.readFileSync(filePath).toString());
}

const packageJson = load('package.json');
const packageVersion = packageJson.version;

if (!/^[0-9]+\.[0-9]+\.[0-9]+-beta\.[0-9]+$/gi.test(packageVersion)) {
    console.error(`Invalid version (${packageVersion}) - must be in the format 9.9.9-beta.9 (ex: 1.0.0-beta.1)`);
    exit(1);
}

const packageLockJson = load('package-lock.json');
const packageLockVersion = packageLockJson.version;

if (packageVersion !== packageLockVersion) {
    console.error(`Different versions in package.json (${packageVersion}) and package-lock.json (${packageLockVersion})`);
    exit(1);
}

exit(0);