const { exit } = require('process');
const fs = require('fs');
const path = require('path');

function load(fileName) {
    const filePath = path.join(__dirname, '..', fileName);
    return JSON.parse(fs.readFileSync(filePath).toString());
}

const packageJson = load('package.json');
const packageVersion = packageJson.version;

if (/-beta\./gi.test(packageVersion)) {
    console.error(`Invalid version (${packageVersion}) - must not have beta suffix for production release`);
    exit(1);
}

exit(0);