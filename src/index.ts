module.exports = {
    printWidth: 130,
    tabWidth: 4,
    useTabs: false,
    semi: true,
    singleQuote: true,
    quoteProps: 'as-needed',
    trailingComma: 'none',
    bracketSpacing: true,
    arrowParens: 'always',
    proseWrap: 'preserve',
    endOfLine: 'lf',
}
