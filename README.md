# @ephesoft/prettier-config

Prettier configuration for Ephesoft apps. Use together with [@ephesoft/eslint-plugin](https://bitbucket.org/ephesoft/eslint-plugin).

## Installation

```sh
npm install --save-dev eslint prettier @ephesoft/eslint-plugin @ephesoft/prettier-config
```

## Configuration

Add the following to `package.json`.

```json
{
  ...
  "scripts": {
    "lint": "eslint --ignore-path .gitignore --ext .ts .",
    "lint:fix": "npm run lint -- --fix",
    "lint:report": "npm run lint -- -f json -o ./lint/lint_report.json"
  },
  "eslintConfig": {
    "extends": [
      "plugin:@ephesoft/typescript"
    ],
    "parserOptions": {
      "project": [
        "tsconfig.json",
        "test/tsconfig.json"
      ]
    }
  },
  "prettier": "@ephesoft/prettier-config",
```

## Git hooks

You can also use this package with `husky` and `lint-staged` to lint files before committing.

```sh
npm install --save-dev husky lint-staged
```

Add the following to `package.json`.

```json
{
  ...
  "lint-staged": {
    "*.ts": "eslint --fix"
  },
  ...
}
```
