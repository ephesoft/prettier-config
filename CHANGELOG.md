# CHANGELOG

## 1.7.0 (2022-10-21)
- Removed internal package references

## 1.6.0 (2022-10-04)
- Updated to template 6.0.2
- Updated npm references

## 1.5.0 (2022-09-26)
- Updated build to be more inline with other packages

## 1.4.0 (2022-09-26)
- Updated to node 16
- Updated npm references
- Updated to template version 5.2.0
- Updated bitbucket-pipelines.yml to get rid of npm audit warning

## 1.3.1 (2021-07-02)
- Fixed npm audit issues

## 1.3.0 (2020-09-18)
- Updated npm references

## 1.2.0 (2020-09-04)
- Updated npm references

## 1.1.1 (2020-08-25)
- Updated to use husky and updated documentation

## 1.1.0 (2020-08-25)
- Updated to latest prettier version
- Added support for beta builds
- Added validation to make sure beta versions aren't published as GA
- Added missing configuration files and scripts from template
- Added workspace file for VSCode
- Added package tests
- Added .mocharc.js file
- Updated build to more closely match template version

## 1.0.0 (2020-04-03)
- Added initial files

- - - - -
* Following [Semantic Versioning](https://semver.org/)
* [Changelog Formatting](https://ephesoft.atlassian.net/wiki/spaces/ZEUS1/pages/1189347481/Changelog+Formatting)
* [Major Version Migration Notes](MIGRATION.md)
